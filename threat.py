import json
import urllib.request
import zipfile
import xml.etree.ElementTree as ET
from jira import JIRA

# Download NIST JSON
nist_json = urllib.request.urlretrieve("https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-modified.json.zip", "nvdcve-1.0-modified.json.zip")


# Unzip NIST JSON
with zipfile.ZipFile("nvdcve-1.0-modified.json.zip", 'r') as zip_ref:
    zip_ref.extractall(".")



# Parse NIST JSON
with open("nvdcve-1.0-modified.json", encoding="utf8") as data:
    json_data = json.load(data)
    cve_list = json_data['CVE_Items']


# Create a list of exploitable CVES:
exploitable_cves = []
for cve in cve_list:
    for reference_data in cve['cve']['references']['reference_data']:
        for tag in reference_data['tags']:
            if tag == "Exploit":
              exploitable_cves.append(cve['cve']['CVE_data_meta']['ID'])


# Parse the Nessus report for the list of vulnerabilities
vulnerable_cves = []
tree = ET.parse('nessus_report.nessus')
root = tree.getroot()
for report in root:
    for reportHost in report:
        for detail in reportHost:
            if detail.tag == "ReportItem":
                for inner_detail in detail:
                    if (inner_detail.tag == "cve"):
                        vulnerable_cves.append(inner_detail.text)

# Fusing both lists Together
fused_list = [vulnerable_cve for vulnerable_cve in vulnerable_cves if vulnerable_cve in exploitable_cves]

jira = JIRA("https://domain.atlassian.net", auth=('user@domain.com', 'secretpassword'))

for cve in fused_list:
    issue_dict = {
        'project': {'key': 'TEST Project'},
        'summary': 'Exploitable CVE found!',
        'description': f'Exploitable CVE: {cve}',
        'issuetype': {'name': 'Task'},
    }
    jira.create_issue(fields=issue_dict)

jira = JIRA("https://domain.atlassian.net", auth=('user@domain.com', 'secretpassword'))

for cve in fused_list:
    issue_dict = {
        'project': {'key': 'TEST Project'},
        'summary': 'Exploitable CVE found!',
        'description': f'Exploitable CVE: {cve}',
        'issuetype': {'name': 'Task'},
    }
    jira.create_issue(fields=issue_dict)


jira = JIRA("https://domain.atlassian.net", auth=('user@domain.com', 'secretpassword'))

for cve in fused_list:
    issue_dict = {
        'project': {'key': 'TEST Project'},
        'summary': 'Exploitable CVE found!',
        'description': f'Exploitable CVE: {cve}',
        'issuetype': {'name': 'Task'},
    }
    jira.create_issue(fields=issue_dict)


jira = JIRA("https://domain.atlassian.net", auth=('user@domain.com', 'secretpassword'))

for cve in fused_list:
    issue_dict = {
        'project': {'key': 'TEST Project'},
        'summary': 'Exploitable CVE found!',
        'description': f'Exploitable CVE: {cve}',
        'issuetype': {'name': 'Task'},
    }
    jira.create_issue(fields=issue_dict)
